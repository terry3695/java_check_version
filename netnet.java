package DeductCoupon;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import mypackage.CalTool;
import mypackage.GenericConnection;
import mypackage.SortObject;

public class Main
{
  String targetDir = "";
  String defaultINI = ".\\NetNetsales.ini";
  String logDir = ".\\Log_NetNetsales";
  String DBname;
  String DBurl;
  String DBdriverclass;
  String DBusername;
  String DBpassword;
  String PromTheme;
  String From = "System Auto-Generation";
  private DecimalFormat df = new DecimalFormat("#,###,##0");
  private String seperate = "\t**************************************************";
  
  protected static void update_db(Connection con, String update, String printout)
  {
    try
    {
      PreparedStatement psn = null;
      psn = con.prepareStatement(update);
      System.out.println(printout);
      psn.execute();
      psn.close();
    }
    catch (Exception localException) {}
  }
  
  private Connection getConn()
  {
    Connection con = null;
    try
    {
      con = SystemUtility.getConnection(this);
    }
    catch (Exception localException) {}
    return con;
  }
  
  private void closeConn(Connection con)
  {
    try
    {
      con.close();
    }
    catch (SQLException localSQLException) {}
  }
  
  public void readInfo()
  {
    try
    {
      Properties p = new Properties();
      p.load(new FileInputStream(this.defaultINI));
      
      this.PromTheme = p.getProperty("PromTheme");
      
      this.DBname = p.getProperty("DBname");
      this.DBurl = p.getProperty("DBurl");
      this.DBdriverclass = p.getProperty("DBdriverclass");
      this.DBusername = p.getProperty("DBusername");
      this.DBpassword = p.getProperty("DBpassword");
      
      System.out.println("Promotion Theme: " + this.PromTheme);
      
      System.out.println("DBname: " + this.DBname);
      System.out.println("DBurl: " + this.DBurl);
      System.out.println("DBdriverclass: " + this.DBdriverclass);
      System.out.println("DBusername: " + this.DBusername);
      System.out.println("DBpassword: " + this.DBpassword);
    }
    catch (FileNotFoundException localFileNotFoundException) {}catch (IOException localIOException) {}
  }
  
  protected static void Add_Check(String d, ArrayList invcp, ArrayList newfilelist, String msgcp, Double Temp_amt)
  {
    System.out.println("hihimsgcp:" + msgcp);
    String[] _msgcp = null;
    String[] _msgcp2 = null;
    Boolean inv_add = Boolean.valueOf(false);
    Boolean found = Boolean.valueOf(false);
    String tempmsgcp = "";
    if (msgcp.contains(","))
    {
      _msgcp = msgcp.split(",");
      if (_msgcp != null) {
        tempmsgcp = _msgcp[0];
      }
    }
    if ((_msgcp != null) && 
      (_msgcp[0].contains("-")))
    {
      _msgcp2 = _msgcp[0].split("-");
      if (_msgcp2.length > 0) {
        tempmsgcp = _msgcp2[0];
      }
    }
    if (msgcp.contains("BP")) {}
    CouponObject CO = new CouponObject(tempmsgcp, Temp_amt.doubleValue(), false, "1", Double.valueOf(0.0D), Double.valueOf(0.0D), 0.0D);
    System.out.println(" msgcp:" + msgcp);
    if ((!tempmsgcp.contains("-X")) || (msgcp.equalsIgnoreCase("BP")))
    {
      for (int c = 0; c < invcp.size(); c++)
      {
        CouponObject ACO = (CouponObject)invcp.get(c);
        if (ACO.getcoupoN().equalsIgnoreCase(CO.getcoupoN()))
        {
          ACO.setamT(ACO.getamT() + CO.getamT());
          found = Boolean.valueOf(true);
        }
      }
      for (int d2 = 0; (d2 < newfilelist.size()) && (!inv_add.booleanValue()); d2++)
      {
        String[] list = newfilelist.get(d2).toString().split(d);
        if (CO.getcoupoN().contains(list[0].trim()))
        {
          CO.setprioritY(list[7].trim());
          CO.setrealDeductPerc(Double.parseDouble(list[8].trim()));
          CO.setRebatePerc(Double.parseDouble(list[9].trim()));
          
          inv_add = Boolean.valueOf(true);
        }
      }
    }
    if ((!found.booleanValue()) && ((inv_add.booleanValue()) || (msgcp.equalsIgnoreCase("BP")))) {
      invcp.add(CO);
    }
  }
  
  public static void main(String[] args)
  {
    String folder = "c:\\8171\\coupon\\";
    String date = "";
    String d = "#@#";
    DecimalFormat df3 = new DecimalFormat("####0.00");
    ArrayList newfilelist = new ArrayList();
    CouponObject CO = null;
    CouponObject ACO = null;
    CouponObject CCO = null;
    InvoiceLineObject ILO = null;
    InvoiceLineObject FILO = null;
    SKUObject SO = null;
    SKUObject SO2 = null;
    String txmsgD = new String(new char[] { '\013' });
    String ExcludeSKU = "'6000364','2000317','2000471'";
    try
    {
      System.out.println("[" + TimeStamp.getTimeStamp() + "] Deduct Coupon Sales Version 1.0");
      
      Connection con = GenericConnection.getConnectionOracle();
      
      Statement stmt = con.createStatement();
      Statement stmt2 = con.createStatement();
      Statement stmt3 = con.createStatement();
      Statement stmt4 = con.createStatement();
      ArrayList cpno = new ArrayList();
      String q = "select distinct (cpno) from it_coupon_master order by cpno";
      ResultSet rs = stmt.executeQuery(q);
      while (rs.next())
      {
        rs.getString("cpno");
        cpno.add(rs.getString("cpno"));
      }
      String cpnoString = "";
      for (int cp = 0; cp < cpno.size(); cp++)
      {
        String tempcp = cpno.get(cp).toString();
        if (cp > 0) {
          cpnoString = cpnoString + "or ";
        }
        cpnoString = cpnoString + "xf_txmsg like'%" + tempcp + "%'";
      }
      String q9 = "select cpno,title,value,prod_type,brand,sku,Remark,case when realdeduct_perc is null then 0 else realdeduct_perc end realdeduct_perc,case when rebate_perc is null then 0 else rebate_perc end rebate_perc,case  when (PROD_TYPE ='ALL' and BRAND='ALL' and SKU='ALL')then 5 when (PROD_TYPE <>'ALL' and BRAND='ALL' and SKU='ALL')then 4 when (PROD_TYPE ='ALL' and BRAND<>'ALL' and SKU='ALL')then 3 when (PROD_TYPE <>'ALL' and BRAND<>'ALL' and SKU='ALL')then 2 when (SKU<>'ALL')then 1 end priority  from it_coupon_master ";
      
      rs = stmt.executeQuery(q9);
      int i = 0;
      while (rs.next())
      {
        i++;
        String cp_no = rs.getString("cpno").trim();
        String title = rs.getString("title");
        String value = rs.getString("value");
        String prod_type = rs.getString("prod_type");
        String brand = rs.getString("brand");
        String sku = rs.getString("sku");
        String Remark = rs.getString("Remark");
        String Priority = rs.getString("Priority");
        double realdeduct_perc = rs.getDouble("realdeduct_perc");
        double rebate_perc = rs.getDouble("rebate_perc");
        
        newfilelist.add(cp_no + d + title + d + value + d + prod_type + d + brand + d + sku + d + Remark + d + Priority + d + realdeduct_perc + d + rebate_perc);
      }
      try
      {
        String PromMaster = "select * from it_prom_less_master where startdate<=sysdate and enddate>=sysdate";
        System.out.println("q8:" + PromMaster);
        ResultSet rs8 = stmt4.executeQuery(PromMaster);
        if (rs8.next())
        {
          String promid = rs8.getString("promid");
          String sql = "update IT_TRANS_PROM set hitamt= case when (localcheck is null or localcheck =0) and p1 is not null and p1='" + promid + "' then amt1*2/3 " + "when (localcheck is null or localcheck =0) and p2 is not null and p2='" + promid + "' then amt2*2/3 " + "when (localcheck is null or localcheck =0) and p3 is not null and p3='" + promid + "' then amt3*2/3 " + "when (localcheck is null or localcheck =0) and p4 is not null and p4='" + promid + "' then amt4*2/3 " + "when (localcheck is null or localcheck =0) and p5 is not null and p5='" + promid + "' then amt5*2/3 " + "when (localcheck is null or localcheck =0) and p6 is not null and p6='" + promid + "' then amt6*2/3 " + "when (localcheck is null or localcheck =0) and p7 is not null and p7='" + promid + "' then amt7*2/3 " + "when (localcheck is null or localcheck =0) and p8 is not null and p8='" + promid + "' then amt8*2/3 " + "when (localcheck is null or localcheck =0) and p9 is not null and p9='" + promid + "' then amt9*2/3 " + "when (localcheck is null or localcheck =0) and p10 is not null and p10='" + promid + "' then amt10*2/3 end , " + "localcheck=1 where localcheck is null or localcheck =0";
          
          System.out.println("sql: " + sql);
          stmt.executeQuery(sql);
          sql = "update IT_TRANS_PROM set status=0 where hitamt <>0 and status is null";
          System.out.println("sql: " + sql);
          stmt.executeQuery(sql);
        }
        String PromDeduct = "select to_char(txdate,'yyyy-mm-dd') as txdate ,storecode,docno,voiddocno,line,hitamt,status from it_trans_prom  where status ='0' and docno like 'S%' ";
        
        System.out.println("PromDeduct: " + PromDeduct);
        ResultSet rs6 = stmt3.executeQuery(PromDeduct);
        while (rs6.next())
        {
          String txdate = rs6.getString("txdate");
          String storecode = rs6.getString("storecode");
          String docno = rs6.getString("docno");
          String voiddocno = rs6.getString("voiddocno");
          String line = rs6.getString("line");
          double hitamt = rs6.getDouble("hitamt");
          
          System.out.println("txdate: " + txdate + " storecode " + storecode + " docno " + docno + " line " + line + " hitamt " + hitamt + " ");
          
          String updates = "";
          String sql = "";
          if (hitamt > 0.0D)
          {
            sql = "merge into it_vipitemdm_netnetsales a\n using  \n(SELECT  txdate,  storecode,  docno, sku, line,hitamt FROM it_trans_prom where txdate=to_date('" + txdate + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno='" + docno + "' and line=" + line + " and hitamt>0) b \n on (a.xf_txdate=b.txdate and a.xf_storecode=b.storecode and a.xf_docno=b.docno and a.xf_saleslinenum=b.line) \n when matched then \n update SET a.it_SpecialPromLess = b.hitamt*-1,a.xf_amtsold=a.xf_amtsold+b.hitamt , a.it_netnetcopy=a.xf_amtsold+b.hitamt where a.xf_amtsold>0\n";
            System.out.println("sql: " + sql);
            stmt.executeQuery(sql);
            
            updates = "update it_trans_prom set status=1  where txdate=to_date('" + txdate + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'and line=" + line + " and hitamt>0";
            update_db(con, updates, "PromLess positive value updated:" + updates);
          }
          else
          {
            System.out.println("sql: " + sql);
            sql = "merge into it_vipitemdm_netnetsales a\n using  \n(SELECT  txdate,  storecode,  docno, sku, line,hitamt FROM it_trans_prom where txdate=to_date('" + txdate + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno='" + docno + "' and line=" + line + " and hitamt<0) b \n on (a.xf_txdate=b.txdate and a.xf_storecode=b.storecode and a.xf_docno=b.docno and a.xf_saleslinenum=b.line) \n when matched then \n update SET a.it_SpecialPromLess = b.hitamt*-1 ,a.xf_amtsold=a.xf_amtsold+b.hitamt , a.it_netnetcopy=a.xf_amtsold+b.hitamt where a.xf_amtsold<0\n";
            stmt.executeQuery(sql);
            
            updates = "update it_trans_prom set status=1  where txdate=to_date('" + txdate + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'and line=" + line + "and hitamt<0 ";
            update_db(con, updates, "refund:" + updates);
          }
        }
        PromDeduct = "select distinct to_char(txdate,'yyyy-mm-dd') as txdate ,storecode,docno,voiddocno from it_trans_prom  where status ='0' and docno like 'V%' ";
        rs6 = stmt3.executeQuery(PromDeduct);
        while (rs6.next())
        {
          String txdate = rs6.getString("txdate");
          String storecode = rs6.getString("storecode");
          String docno = rs6.getString("docno");
          String voiddocno = rs6.getString("voiddocno");
          
          System.out.println("txdate: " + txdate + " storecode " + storecode + " docno " + docno);
          
          String updates = "";
          if (docno.startsWith("V"))
          {
            String sql = "merge into it_vipitemdm_netnetsales a\n using  \n(SELECT distinct txdate,  storecode,  voiddocno FROM it_trans_prom where txdate=to_date('" + txdate + "','yyyy-mm-dd') and storecode='" + storecode + "' and voiddocno='" + voiddocno + "' ) b \n on (a.xf_txdate=b.txdate and a.xf_storecode=b.storecode and a.xf_docno=b.voiddocno ) \n when matched then \n update SET a.it_SpecialPromLess = 0 ,a.xf_amtsold=it_orig_netsales  \n";
            System.out.println("sql -value: " + sql);
            stmt.executeQuery(sql);
            
            updates = "update it_trans_prom set status=2  where txdate=to_date('" + txdate + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
            update_db(con, updates, "Voided:" + updates);
          }
        }
        String ce = "select to_char(txdate,'yyyy-mm-dd') as txdate ,storecode,docno from it_coupon_exception  where status is null or status not in (1,2,3)  ";
        ResultSet rs4 = stmt2.executeQuery(ce);
        String storecode = "";
        String docno = "";
        double v_invoicetotal = 0.0D;
        Boolean allcphitted = Boolean.valueOf(true);
        Boolean Voided = Boolean.valueOf(false);
        System.out.println("ce  " + ce);
        while (rs4.next())
        {
          Voided = Boolean.valueOf(false);
          
          date = rs4.getString("txdate");
          storecode = rs4.getString("storecode");
          docno = rs4.getString("docno");
          Boolean invoice_total_match = Boolean.valueOf(false);
          double trans_invoicetotal = 0.0D;
          Boolean cp_hitted = Boolean.valueOf(true);
          Boolean updatesuccess = Boolean.valueOf(false);
          ArrayList invcp = new ArrayList();
          if (docno.startsWith("S"))
          {
            v_invoicetotal = 0.0D;
            String q7 = "SELECT SUM(it_orig_netsales) AS it_orig_netsales  FROM it_vipitemdm_netnetsales where xf_txdate=TO_DATE('" + date + "','YYYY-MM-DD') and xf_storecode='" + storecode + "' AND xf_docno='" + docno + "'";
            System.out.println("q7  " + q7);
            ResultSet rs7 = stmt3.executeQuery(q7);
            if (rs7.next())
            {
              v_invoicetotal = rs7.getDouble("it_orig_netsales");
              System.out.println(" v_invoicetotal: " + v_invoicetotal);
            }
            String q4 = "select xf_txtype,xf_txmsg  from xf_trans where xf_txdate=to_date('" + date + "','YYYY-MM-DD') and xf_storecode='" + storecode + "' and xf_docno='" + docno + "'and xf_txtype in (1,18) ";
            System.out.println("q4  " + q4);
            ResultSet rs5 = stmt3.executeQuery(q4);
            String[] _txmsg = null;
            
            int invcpcount = 0;
            double amt1 = 0.0D;
            double amt2 = 0.0D;
            trans_invoicetotal = 0.0D;
            String msgcp = "";
            String tender1 = "";
            String tender2 = "";
            while (rs5.next())
            {
              invcpcount++;
              String txmsg = rs5.getString("xf_txmsg");
              String txtype = rs5.getString("xf_txtype");
              System.out.println(invcpcount + ".txmsg " + txmsg);
              _txmsg = txmsg.split(txmsgD);
              if (txtype.equals("18"))
              {
                tender1 = _txmsg[1];
                tender2 = "";
                System.out.println(" tender1:" + tender1);
                if (_txmsg.length > 10)
                {
                  tender2 = _txmsg[7];
                  System.out.println(" tender2:" + tender2);
                }
                if ((tender1.equals("CP")) || (tender1.equals("C4")) || (tender1.equals("C7")) || (tender1.equals("C8")))
                {
                  amt1 = Double.parseDouble(_txmsg[4]);
                  System.out.println("tender amt1:" + amt1);
                  msgcp = _txmsg[6];
                  String tempmsgcp = msgcp;
                  if (tender1.equals("BP")) {
                    tempmsgcp = "BP";
                  }
                  Add_Check(d, invcp, newfilelist, tempmsgcp, Double.valueOf(amt1));
                }
                if ((tender2.equals("CP")) || (tender2.equals("C4")) || (tender2.equals("C7")) || (tender2.equals("C8")))
                {
                  System.out.println("_txmsg[10]:" + _txmsg[10]);
                  amt2 = Double.parseDouble(_txmsg[10].trim());
                  System.out.println("tender amt2:" + amt2);
                  msgcp = _txmsg[12];
                  String tempmsgcp = msgcp;
                  if (tender2.equals("BP")) {
                    tempmsgcp = "BP";
                  }
                  Add_Check(d, invcp, newfilelist, tempmsgcp, Double.valueOf(amt2));
                }
              }
              if (txtype.equals("1")) {
                trans_invoicetotal = Double.parseDouble(_txmsg[24]);
              }
            }
            for (int t = 0; t < invcp.size(); t++)
            {
              CO = (CouponObject)invcp.get(t);
              System.out.println(t + " " + CO.getcoupoN() + " " + CO.getprioritY());
            }
            Collections.sort(invcp, new SortObject());
            System.out.println("trans_invoicetotal: " + trans_invoicetotal + " v_invoicetotal: " + v_invoicetotal);
            System.out.println("v_invoicetotal-trans_invoicetotal: " + (v_invoicetotal - trans_invoicetotal) + "  v_invoicetotal-trans_invoicetotal" + (v_invoicetotal - trans_invoicetotal));
            if ((trans_invoicetotal == v_invoicetotal) || ((0.09D >= v_invoicetotal - trans_invoicetotal) && (v_invoicetotal - trans_invoicetotal >= -0.09D))) {
              invoice_total_match = Boolean.valueOf(true);
            }
            if (trans_invoicetotal > 0.0D)
            {
              String txtype = "";
              String cp = "";
              String cp_matched = "";
              String value = "";
              String prod_type = "";
              String brand = "";
              String sku = "";
              double amtsold = 0.0D;
              double orig_netsales = 0.0D;
              double invoicetotal = 0.0D;
              double cp_ttl = 0.0D;
              String invoice_sku = "";
              String line = "";
              String invoice_brand = "";
              String invoice_prod_type = "";
              String sub_prod_type = "";
              amtsold = 0.0D;
              orig_netsales = 0.0D;
              int itemcount = 0;
              double invoice_cp_ttl = 0.0D;
              double invoice_cp_deduct_ttl = 0.0D;
              String invoice_cpno = "";
              txtype = "";
              for (int c = 0; c < invcp.size(); c++)
              {
                CCO = (CouponObject)invcp.get(c);
                invoice_cp_ttl = CCO.getamT();
                System.out.print("coupon no:" + CCO.getcoupoN() + " realDeductPerc:" + CCO.getrealDeductPerc() + " RebatePerc:" + CCO.getRebatePerc());
                CCO.setRebateamT(CCO.getamT() * CCO.getRebatePerc());
                System.out.print(" Rebate :" + CCO.getRebateamT());
                System.out.print(" original invoice_cp_ttl:" + CCO.getamT());
                if (CCO.getrealDeductPerc() > 0.0D)
                {
                  CCO.setamT(CCO.getamT() * CCO.getrealDeductPerc());
                  System.out.print(" deducted invoice_cp_ttl:" + CCO.getamT());
                }
                System.out.println(" total coupon count:" + invcp.size());
              }
              updatesuccess = Boolean.valueOf(false);
              for (int c = 0; c < invcp.size(); c++)
              {
                ArrayList icArrayList = new ArrayList();
                CCO = (CouponObject)invcp.get(c);
                invoice_cpno = CCO.getcoupoN();
                invoice_cp_ttl = CCO.getamT();
                invoice_cp_deduct_ttl = CCO.getRebateamT();
                System.out.println("coupon loop:" + c + invoice_cpno);
                cp_ttl = 0.0D;
                ArrayList SKUArrayList = new ArrayList();
                String s = "select v.*,m.xf_group1, m.xf_group2,m.xf_group3 from it_vipitemdm_netnetsales v inner join xf_itemmas m on v.xf_style=m.xf_style and m.xf_group2 <>'18' and v.xf_style not in (" + ExcludeSKU + ") where v.xf_txdate= to_date('" + date + "','yyyy-mm-dd') and v.xf_storecode='" + storecode + "' and v.xf_docno='" + docno + "' order by xf_amtsold";
                Boolean BPflag = Boolean.valueOf(false);
                if (invoice_cpno.equalsIgnoreCase("BP")) {
                  s = "select v.*,m.xf_group1, m.xf_group2,m.xf_group3 from it_vipitemdm_netnetsales v inner join xf_itemmas m on v.xf_style=m.xf_style and m.xf_group2 <>'18' and v.xf_style not in (" + ExcludeSKU + ") where v.xf_txdate= to_date('" + date + "','yyyy-mm-dd') and v.xf_storecode='" + storecode + "' and v.xf_docno='" + docno + "' order by xf_amtsold";
                }
                System.out.println("invoice_cpno: " + invoice_cpno);
                System.out.println("BPflag: " + BPflag);
                rs5 = stmt3.executeQuery(s);
                Double InvPosCpTtl = Double.valueOf(0.0D);
                while (rs5.next())
                {
                  Boolean itemmatch = Boolean.valueOf(false);
                  itemcount++;
                  invoice_sku = rs5.getString("xf_style");
                  line = rs5.getString("xf_saleslinenum");
                  invoice_brand = rs5.getString("xf_group1").trim();
                  invoice_prod_type = rs5.getString("xf_group2").trim();
                  sub_prod_type = rs5.getString("xf_group3").trim();
                  amtsold = rs5.getDouble("xf_amtsold");
                  orig_netsales = rs5.getDouble("it_orig_netsales");
                  txtype = rs5.getString("xf_txtype");
                  invoicetotal += orig_netsales;
                  System.out.println("list2: " + itemcount + ":" + invoice_sku + " " + line + " " + invoice_brand + " " + invoice_prod_type + " " + sub_prod_type + " " + orig_netsales + " " + invoicetotal + " newfilelist.size():" + newfilelist.size());
                  for (int d2 = 0; (d2 < newfilelist.size()) && (!BPflag.booleanValue()); d2++)
                  {
                    String[] list = newfilelist.get(d2).toString().split(d);
                    if (CCO.getcoupoN().contains(list[0]))
                    {
                      cp = list[0].toString().trim();
                      value = list[2].toString();
                      prod_type = list[3].toString().trim();
                      brand = list[4].toString().trim();
                      sku = list[5].toString().trim();
                      if (sku.equals(invoice_sku))
                      {
                        System.out.println("SKU matched~:" + cp + " " + value + " " + prod_type + " " + brand + " " + sku);
                        itemmatch = Boolean.valueOf(true);
                        cp_matched = cp;
                      }
                      if ((!itemmatch.booleanValue()) && (prod_type.equalsIgnoreCase("ALL")) && (brand.equals(invoice_brand)))
                      {
                        System.out.println("brand matched all product type~:" + cp + " " + value + " " + prod_type + " " + brand + " " + sku);
                        itemmatch = Boolean.valueOf(true);
                        cp_matched = cp;
                      }
                      if (!prod_type.equalsIgnoreCase("ALL"))
                      {
                        if ((!itemmatch.booleanValue()) && ((prod_type.equals(invoice_prod_type)) || (prod_type.equals(sub_prod_type))) && (brand.equals(invoice_brand)))
                        {
                          System.out.println("brand and product type matched~:" + cp + " " + value + " " + prod_type + " " + brand + " " + sku);
                          itemmatch = Boolean.valueOf(true);
                          cp_matched = cp;
                        }
                        if ((!itemmatch.booleanValue()) && ((prod_type.equals(invoice_prod_type)) || (prod_type.equals(sub_prod_type))) && (brand.equalsIgnoreCase("ALL")))
                        {
                          System.out.println("product type matched~:" + cp + " " + value + " " + prod_type + " " + brand + " " + sku);
                          itemmatch = Boolean.valueOf(true);
                          cp_matched = cp;
                        }
                      }
                      if ((prod_type.equalsIgnoreCase("ALL")) && (brand.equalsIgnoreCase("ALL")) && (sku.equalsIgnoreCase("ALL")))
                      {
                        System.out.println("ALL matched~:" + cp + " " + value + " " + prod_type + " " + brand + " " + sku);
                        itemmatch = Boolean.valueOf(true);
                        cp_matched = cp;
                      }
                    }
                  }
                  if (BPflag.booleanValue())
                  {
                    System.out.println("ALL matched BP~:" + invoice_sku);
                    itemmatch = Boolean.valueOf(true);
                    cp_matched = "BP";
                  }
                  if (itemmatch.booleanValue())
                  {
                    System.out.print("invoice by coupon: :" + invoice_cpno);
                    System.out.println(" invoice_cp_ttl:" + invoice_cp_ttl);
                    System.out.println("coupon itemmatch:");
                    cp_ttl += orig_netsales;
                    String tempinvoice_cpno = "";
                    if (!BPflag.booleanValue()) {
                      tempinvoice_cpno = invoice_cpno;
                    } else {
                      tempinvoice_cpno = "BP";
                    }
                    ILO = new InvoiceLineObject(tempinvoice_cpno, invoice_sku, line, orig_netsales, amtsold, 0.0D);
                    icArrayList.add(ILO);
                    Boolean SKUfound = Boolean.valueOf(false);
                    double temporig_netsales = 0.0D;
                    if (orig_netsales > 0.0D) {
                      temporig_netsales = orig_netsales;
                    }
                    SO = new SKUObject(invoice_sku, orig_netsales, temporig_netsales);
                    for (int ss = 0; ss < SKUArrayList.size(); ss++)
                    {
                      SO2 = (SKUObject)SKUArrayList.get(ss);
                      if (SO2.getskU().equals(invoice_sku))
                      {
                        SO2.setamT(SO2.getamT() + orig_netsales);
                        SKUfound = Boolean.valueOf(true);
                        if (orig_netsales > 0.1D)
                        {
                          CCO.sethiT(true);
                          SO2.setposamT(SO2.getposamT() + SO.getposamT());
                          System.out.println("SO2.getposamT() " + SO2.getposamT() + "  orig_netsales " + SO.getposamT());
                        }
                      }
                    }
                    if (!SKUfound.booleanValue())
                    {
                      SKUArrayList.add(SO);
                      if (orig_netsales > 0.0D) {
                        CCO.sethiT(true);
                      }
                    }
                  }
                  else
                  {
                    cp_hitted = Boolean.valueOf(false);
                  }
                }
                for (int ti = 0; ti < SKUArrayList.size(); ti++)
                {
                  SO = (SKUObject)SKUArrayList.get(ti);
                  if (SO.getamT() > 0.0D) {
                    InvPosCpTtl = Double.valueOf(InvPosCpTtl.doubleValue() + SO.getamT());
                  }
                }
                System.out.println(c + " invoice coupon " + invoice_cpno + " icArrayList size: " + icArrayList.size() + " SKUArrayList:" + SKUArrayList.size());
                Double inv_cp_sku_ttl = Double.valueOf(0.0D);
                for (int ti = 0; ti < icArrayList.size(); ti++)
                {
                  FILO = (InvoiceLineObject)icArrayList.get(ti);
                  for (int t2 = 0; t2 < SKUArrayList.size(); t2++)
                  {
                    SO = (SKUObject)SKUArrayList.get(t2);
                    if ((SO.getskU().equalsIgnoreCase(FILO.getskU())) && (SO.getamT() > 0.1D) && (FILO.getamT() > 0.1D) && (FILO.getcoupoN().contains(cp_matched)))
                    {
                      System.out.print("FILO.getamT():" + FILO.getamT());
                      System.out.println("  FILO.getafteRamT():" + FILO.getafteRamT() + " cp_ttl: " + cp_ttl + " invoice_cp_ttl: " + invoice_cp_ttl + " SO.getamT " + SO.getamT() + " SO.getposamT " + SO.getposamT() + " InvPosCpTtl: " + InvPosCpTtl);
                      
                      double temp1 = FILO.getafteRamT();
                      double temp2 = SO.getamT() / InvPosCpTtl.doubleValue() * invoice_cp_ttl * FILO.getamT() / SO.getposamT();
                      double temp3 = SO.getamT() / InvPosCpTtl.doubleValue() * invoice_cp_deduct_ttl * FILO.getamT() / SO.getposamT();
                      System.out.println("result 1: " + temp1);
                      System.out.println("result 2: " + temp2);
                      System.out.println("result 3: " + temp3);
                      FILO.setafteRamT(temp1 - temp2);
                      FILO.setRebateamT(temp3);
                      inv_cp_sku_ttl = Double.valueOf(inv_cp_sku_ttl.doubleValue() + FILO.getafteRamT());
                    }
                  }
                }
                System.out.println("invoice total:" + df3.format(invoicetotal));
                System.out.println("cp_ttl:" + cp_ttl + "invoice_cp_ttl:" + invoice_cp_ttl);
                double remain_value = 0.0D;
                System.out.println("inv_cp_sku_ttl:" + inv_cp_sku_ttl);
                remain_value = inv_cp_sku_ttl.doubleValue();
                for (int ti = 0; ti < icArrayList.size(); ti++)
                {
                  FILO = (InvoiceLineObject)icArrayList.get(ti);
                  for (int t2 = 0; t2 < SKUArrayList.size(); t2++)
                  {
                    SO = (SKUObject)SKUArrayList.get(t2);
                    if (((SO.getskU().equalsIgnoreCase(FILO.getskU())) && (SO.getamT() > 0.1D) && (FILO.getafteRamT() != FILO.getamT())) || ((SO.getamT() == 0.0D) && (FILO.getafteRamT() == 0.0D) && (FILO.getamT() == 0.0D)))
                    {
                      String update = "";
                      double rounded_value = CalTool.round(FILO.getafteRamT(), 2);
                      System.out.println("re:" + ti + 1 + "/" + icArrayList.size() + " remain_value:" + remain_value + " -" + rounded_value);
                      System.out.println("re2:" + ti + 1 + "/" + icArrayList.size() + " " + FILO.getcoupoN() + " " + FILO.gelinE() + " " + FILO.getskU());
                      if (ti == icArrayList.size() - 1) {
                        update = "update it_vipitemdm_netnetsales set xf_amtsold=" + CalTool.round(remain_value, 2) + " , it_lastmodtime=current_date ,it_netnetcopy=" + CalTool.round(remain_value, 2) + " , it_specialpromless=" + FILO.getRebateamT() + " where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno ='" + docno + "'and xf_saleslinenum =" + FILO.gelinE() + " and it_orig_netsales=" + FILO.getamT() + "";
                      } else {
                        update = "update it_vipitemdm_netnetsales set xf_amtsold=" + rounded_value + " , it_lastmodtime=current_date ,it_netnetcopy=" + rounded_value + ", it_specialpromless=" + FILO.getRebateamT() + " where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno ='" + docno + "'and xf_saleslinenum =" + FILO.gelinE() + " and it_orig_netsales=" + FILO.getamT() + "";
                      }
                      update_db(con, update, FILO.getamT() + "update:" + update);
                      remain_value -= rounded_value;
                      updatesuccess = Boolean.valueOf(true);
                    }
                  }
                }
              }
              if (updatesuccess.booleanValue())
              {
                String updates = "update it_coupon_exception set status=1  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
                update_db(con, updates, "update complete:" + updates);
              }
            }
          }
          if (docno.startsWith("V"))
          {
            String update = "update it_vipitemdm_netnetsales set xf_amtsold=it_orig_netsales , it_lastmodtime=sysdate, it_specialpromless=0 where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno =(select distinct (xf_voiddocno) from xf_trans where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno='" + docno + "')";
            update_db(con, update, "void update: " + update);
            String updates = "update it_coupon_exception set status=2  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno =(select distinct (xf_voiddocno) from xf_trans where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno='" + docno + "')";
            update_db(con, updates, "update complete:" + updates);
            updates = "update it_coupon_exception set status=2  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
            update_db(con, updates, "update complete:" + updates);
            updates = "update it_coupon_log set status=1  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno =(select distinct (xf_voiddocno) from xf_trans where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno='" + docno + "')";
            update_db(con, updates, "update complete:" + updates);
            Voided = Boolean.valueOf(true);
          }
          if ((!invoice_total_match.booleanValue()) && (docno.startsWith("S")))
          {
            String checkv = "select xf_voiddocno from xf_trans where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_voiddocno ='" + docno + "'";
            System.out.println("checkv: " + checkv);
            ResultSet rs5 = stmt3.executeQuery(checkv);
            if (rs5.next())
            {
              String update = "update it_vipitemdm_netnetsales set xf_amtsold=it_orig_netsales , it_lastmodtime=sysdate, it_specialpromless=0 where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno ='" + docno + "'";
              update_db(con, update, "invoice void update: " + date + storecode + docno);
              String updates = "update it_coupon_exception set status=2  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
              update_db(con, updates, "update complete:" + updates);
              Voided = Boolean.valueOf(true);
            }
            else
            {
              String update = "update it_vipitemdm_netnetsales set xf_amtsold=it_orig_netsales , it_lastmodtime=null, it_specialpromless=0 where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno ='" + docno + "'";
              update_db(con, update, "invoice incomplete update: " + date + storecode + docno);
              String updates = "update it_coupon_exception set status=0  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
              update_db(con, updates, "update complete:" + updates);
            }
          }
          if ((invoice_total_match.booleanValue()) && (trans_invoicetotal < 0.0D))
          {
            String update = "update it_vipitemdm_netnetsales set xf_amtsold=it_orig_netsales, it_specialpromless=0  where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno ='" + docno + "'";
            update_db(con, update, "invoice refund  update: " + date + storecode + docno);
            String updates = "update it_coupon_exception set status=1  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
            update_db(con, updates, "update complete:" + updates);
          }
          if ((!Voided.booleanValue()) && (trans_invoicetotal > 0.0D) && (!invoice_total_match.booleanValue()) && (!updatesuccess.booleanValue()) && (!cp_hitted.booleanValue()))
          {
            String update = "update it_vipitemdm_netnetsales set xf_amtsold=it_orig_netsales ,it_lastmodtime=null, it_specialpromless=0 where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno ='" + docno + "'";
            update_db(con, update, "invoice incomplete update: " + date + storecode + docno);
            String updates = "update it_coupon_exception set status=4  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
            update_db(con, updates, "update complete:" + updates);
          }
          if ((invoice_total_match.booleanValue()) && (!updatesuccess.booleanValue()) && (!cp_hitted.booleanValue()) && (trans_invoicetotal > 0.0D))
          {
            String update = "update it_vipitemdm_netnetsales set xf_amtsold=it_orig_netsales, it_specialpromless=0  where xf_txdate=to_date('" + date + "','yyyy-mm-dd') and xf_storecode='" + storecode + "' and xf_docno ='" + docno + "'";
            update_db(con, update, "coupon not hit update: " + date + storecode + docno);
            String updates = "update it_coupon_exception set status=3  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
            update_db(con, updates, "update complete:" + updates);
          }
          allcphitted = Boolean.valueOf(true);
          for (int c = 0; c < invcp.size(); c++)
          {
            CCO = (CouponObject)invcp.get(c);
            System.out.println(c + "coupon loop:" + CCO.getcoupoN() + CCO.gethiT());
            if (!CCO.gethiT()) {
              allcphitted = Boolean.valueOf(false);
            }
          }
          if ((!allcphitted.booleanValue()) && (!Voided.booleanValue()) && (trans_invoicetotal > 0.0D))
          {
            System.out.println("not all coupon hit update: " + date + storecode + docno);
            for (int c = 0; c < invcp.size(); c++)
            {
              System.out.println("coupon loop:" + c);
              CCO = (CouponObject)invcp.get(c);
              String tempststaus = "1";
              if (!CCO.gethiT()) {
                tempststaus = "0";
              }
              String insert = "insert into  it_coupon_log values( to_date('" + date + "','yyyy-mm-dd'),'" + storecode + "','" + docno + "','" + CCO.getcoupoN() + "'," + CCO.getamT() + "," + tempststaus + ")";
              update_db(con, insert, "update complete:" + insert);
            }
            String updates = "update it_coupon_exception set status=3  where txdate=to_date('" + date + "','yyyy-mm-dd') and storecode='" + storecode + "' and docno ='" + docno + "'";
            update_db(con, updates, "not all coupon hitted:" + updates);
          }
        }
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
      System.out.println("[" + TimeStamp.getTimeStamp() + "] Program End");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
