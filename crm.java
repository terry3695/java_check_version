package PromCrm_UpdateIsItemRedeem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class Main
{
  Date d = new Date();
  String targetDir = "";
  String defaultINI = ".\\PromCrm_UpdateIsItemRedeem.ini";
  String logDir = ".\\Log_PromCrm";
  String DBname;
  String DBurl;
  String DBdriverclass;
  String DBusername;
  String DBpassword;
  String PromTheme;
  String From = "System Auto-Generation";
  private PrintWriter out;
  private static double logKeepDate = 365.0D;
  private final String DATE_FORMAT_NOW = "yyyy-MM-dd";
  private final String DATE_FORMAT_NOW2 = "yyyy-MM-dd HH:mm:ss";
  private final String DATE_FORMAT_NOW3 = "yyyy-MM-dd HH:mm:ss.sss";
  private final String DATE_FORMAT_NOW4 = "yyyyMMdd";
  private final String DATE_FORMAT_NOW5 = "HHmmss";
  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
  SimpleDateFormat sdf4 = new SimpleDateFormat("yyyyMMdd");
  SimpleDateFormat sdf5 = new SimpleDateFormat("HHmmss");
  private DecimalFormat df = new DecimalFormat("#,###,##0");
  private String seperate = "\t**************************************************";
  String reportPrefix = "POS_Rpt_";
  String generalSubject = "POS_Rpt_";
  String generalBody = "This is a system auto-generated message. Please do NOT reply.<br/>";
  String subject;
  String body;
  final int NoOfReport = 9;
  final String trans_table = "xf_trans";
  final String vipitemdm_table = "it_vipitemdm_netnetsales";
  final String prom_table = "xf_promitem";
  final String updCreateTime_table = "it_promCRM_updCreateTime";
  final String staff_table = "xf_staff";
  final String itemmas_table = "xf_itemmas";
  final String vipMapping_table = "it_vipgrade_map";
  String splitTxMsg = String.valueOf(Character.toChars(11));
  String oracleDateF = "yyyy-MM-dd";
  String oracleDateF2 = "yyyy-MM-dd hh24:mi:ss";
  
  protected void createLog()
  {
    try
    {
      new File(this.logDir).mkdir();
      File newLog = new File(this.logDir + "\\LOG_" + (1900 + this.d.getYear()) + "_" + (this.d.getMonth() + 1) + "_" + this.d.getDate() + "_" + this.d.getHours() + "_" + this.d.getMinutes() + "_" + this.d.getSeconds() + ".txt");
      
      this.out = new PrintWriter(new FileWriter(newLog));
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_createLog] Created file");
    }
    catch (Exception e)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_createLog] Error: " + e);
    }
  }
  
  protected void clearLog()
  {
    try
    {
      File logFile = new File(this.logDir);
      String[] files = logFile.list();
      for (int i = 0; i < files.length; i++)
      {
        File eachLog = new File(this.logDir + "\\" + files[i]);
        if (this.d.getTime() - eachLog.lastModified() > 8.64E7D * logKeepDate)
        {
          getLogTime("\t[PromCrm_UpdateIsItemRedeem_clearLog] Deleted Log file: " + eachLog.getAbsolutePath());
          eachLog.delete();
        }
      }
    }
    catch (Exception e)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_clearLog] Error: " + e);
    }
  }
  
  protected void closeLog()
  {
    try
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_closeLog] Close file");
      this.out.close();
    }
    catch (Exception e)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_closeLog] Error: " + e);
    }
  }
  
  public void getLogTime(String log)
  {
    this.d = new Date();
    
    System.out.println(1900 + this.d.getYear() + "-" + (this.d.getMonth() + 1) + "-" + this.d.getDate() + " " + this.d.getHours() + ":" + this.d.getMinutes() + ":" + this.d.getSeconds() + log);
    
    this.out.println(1900 + this.d.getYear() + "-" + (this.d.getMonth() + 1) + "-" + this.d.getDate() + " " + this.d.getHours() + ":" + this.d.getMinutes() + ":" + this.d.getSeconds() + log);
    this.out.flush();
  }
  
  public String getDate(int numOfDayDiff)
  {
    Calendar cal = Calendar.getInstance();
    cal.add(5, numOfDayDiff);
    String date = this.sdf.format(cal.getTime());
    
    return date;
  }
  
  private Connection getConn()
  {
    Connection con = null;
    try
    {
      con = SystemUtility.getConnection(this);
    }
    catch (Exception ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] getConn: Exception: " + ex);
    }
    return con;
  }
  
  private void closeConn(Connection con)
  {
    try
    {
      con.close();
    }
    catch (SQLException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] closeConn: SQLException: " + ex);
    }
  }
  
  public void readInfo()
  {
    getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] Run readInfo().");
    try
    {
      Properties p = new Properties();
      p.load(new FileInputStream(this.defaultINI));
      
      this.PromTheme = p.getProperty("PromTheme");
      
      this.DBname = p.getProperty("DBname");
      this.DBurl = p.getProperty("DBurl");
      this.DBdriverclass = p.getProperty("DBdriverclass");
      this.DBusername = p.getProperty("DBusername");
      this.DBpassword = p.getProperty("DBpassword");
      
      System.out.println("Promotion Theme: " + this.PromTheme);
      
      System.out.println("DBname: " + this.DBname);
      System.out.println("DBurl: " + this.DBurl);
      System.out.println("DBdriverclass: " + this.DBdriverclass);
      System.out.println("DBusername: " + this.DBusername);
      System.out.println("DBpassword: " + this.DBpassword);
      
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] Run readInfo() completed.");
    }
    catch (FileNotFoundException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] Run readInfo: FileNotFoundException: " + ex + ".");
    }
    catch (IOException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] Run readInfo: IOException: " + ex + ".");
    }
  }
  
  public static void main(String[] args)
  {
    Main main = new Main();
    main.createLog();
    main.clearLog();
    main.readInfo();
    
    main.mainprogram();
    
    main.closeLog();
  }
  
  public void mainprogram()
  {
    getLogTime("\t[DB Name] " + this.DBname);
    getLogTime("\t[DB URL] " + this.DBurl);
    getLogTime("\t[DB Username] " + this.DBusername);
    
    Connection con = getConn();
    
    int count_trans = 0;int count_isCrmRedeem = 0;
    ArrayList PromList = new ArrayList();
    
    String[] PromID = new String[10];
    
    updateNegativeCoupon(con);
    try
    {
      String LastTime = getLastTime(con);
      String CurTime = getCurTime(con);
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] LastTime: " + LastTime);
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] CurTime: " + CurTime);
      
      PromList = getPromID(con);
      
      getClass();String sql_getTrans = "SELECT xf_txdate,xf_storecode,xf_docno,xf_voiddocno,xf_txmsg\nFROM " + "xf_trans" + "\n" + "WHERE xf_txtype in (2,3)\n" + "and SUBSTR(xf_docno,1,1) in ('S','V')\n" + "and xf_createtime>=to_date('" + LastTime + "','" + this.oracleDateF2 + "')-(1/24) and xf_createtime<to_date('" + CurTime + "','" + this.oracleDateF2 + "')\n";
      Statement stmt_getTrans = con.createStatement();
      ResultSet rs_getTrans = stmt_getTrans.executeQuery(sql_getTrans);
      while (rs_getTrans.next())
      {
        PromID[0] = null;
        boolean isCrmRedeem = false;
        String sql_updateIsItemRedeem = "";
        
        String TxDate = this.sdf.format(rs_getTrans.getDate("xf_txdate"));
        String StoreCode = rs_getTrans.getString("xf_storecode");
        String DocNo;
        String DocNo;
        if (rs_getTrans.getString("xf_docno").substring(0, 1).toUpperCase().equals("S")) {
          DocNo = rs_getTrans.getString("xf_docno");
        } else {
          DocNo = rs_getTrans.getString("xf_voiddocno");
        }
        String[] txMsg = rs_getTrans.getString("xf_txmsg").split(this.splitTxMsg);
        int LineNo = Integer.parseInt(txMsg[5]);
        String Salesman = txMsg[81];
        String IsItemRedeem = txMsg[83];
        String IsPriceOverride = txMsg[18];
        String VipGrade = txMsg[82];
        String SKU = txMsg[6];
        if ((!VipGrade.toUpperCase().equals("W1")) && (!VipGrade.toUpperCase().equals("W2")))
        {
          for (int i = 0; i < 10; i++) {
            PromID[i] = txMsg[(i * 4 + 29)];
          }
          for (int i = 0; i < PromList.size(); i++)
          {
            String PromID_fromList = (String)PromList.get(i);
            for (int j = 0; j < 10; j++) {
              if (PromID_fromList.toUpperCase().equals(PromID[j].toUpperCase()))
              {
                isCrmRedeem = true;
                break;
              }
            }
            if (isCrmRedeem)
            {
              count_isCrmRedeem++;
              break;
            }
          }
        }
        System.out.println("isCrmRedeem:" + isCrmRedeem + " IsItemRedeem:" + IsItemRedeem);
        if (isCrmRedeem) {
          sql_updateIsItemRedeem = ",xf_isitemredeem='2'\n";
        } else if ((!isCrmRedeem) && (VipGrade.toUpperCase().equals("ZS"))) {
          sql_updateIsItemRedeem = ",xf_isitemredeem='3'\n";
        } else if ((!isCrmRedeem) && (IsItemRedeem.equals("1")) && ((VipGrade.toUpperCase().equals("W1")) || (VipGrade.toUpperCase().equals("W2")))) {
          sql_updateIsItemRedeem = ",xf_isitemredeem='4'\n";
        } else {
          sql_updateIsItemRedeem = "";
        }
        String CustomerClass = getCustomerClass(TxDate, StoreCode, DocNo, Salesman, IsItemRedeem, IsPriceOverride, VipGrade, SKU, PromID[0], con);
        
        getClass();String sql_update = "update " + "it_vipitemdm_netnetsales" + " set\n" + "it_customerclass=?\n" + sql_updateIsItemRedeem + "where xf_txdate=to_date(?,'" + this.oracleDateF + "')\n" + "and xf_storecode=?\n" + "and xf_docno=?\n" + "and xf_saleslinenum=?\n";
        PreparedStatement pstmt = con.prepareStatement(sql_update);
        pstmt.setString(1, CustomerClass);
        pstmt.setString(2, TxDate);
        pstmt.setString(3, StoreCode);
        pstmt.setString(4, DocNo);
        pstmt.setInt(5, LineNo);
        pstmt.executeUpdate();
        pstmt.close();
        
        count_trans++;
      }
      rs_getTrans.close();
      stmt_getTrans.close();
      
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] Number of xf_trans records updated: " + count_trans);
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] Number of CRM Redeem records updated: " + count_isCrmRedeem);
      
      updateLastTime(CurTime, con);
      
      closeConn(con);
    }
    catch (SQLException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] mainprogram: SQLException: " + ex);
    }
  }
  
  public ArrayList getPromID(Connection con)
  {
    ArrayList promList = new ArrayList();
    try
    {
      Statement stmt = con.createStatement();
      getClass();String sql = "select DISTINCT xf_promid from " + "xf_promitem" + " where xf_promtheme='" + this.PromTheme + "'\n" + "and xf_fromdate<=add_months(sysdate,1) and xf_todate>=add_months(sysdate,-1)";
      ResultSet rs = stmt.executeQuery(sql);
      while (rs.next()) {
        promList.add(rs.getString("xf_promid"));
      }
      rs.close();
      stmt.close();
    }
    catch (SQLException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] getPromID: SQLException: " + ex);
    }
    return promList;
  }
  
  public String getLastTime(Connection con)
  {
    String LastTime = null;
    try
    {
      Statement stmt = con.createStatement();
      getClass();String sql = "select MAX(to_char(IT_LASTUPDATETIME,'" + this.oracleDateF2 + "')) from " + "it_promCRM_updCreateTime" + "\n";
      ResultSet rs = stmt.executeQuery(sql);
      if (rs.next()) {
        LastTime = rs.getString(1);
      }
      rs.close();
      stmt.close();
    }
    catch (Exception ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] getLastTime: SQLException: " + ex);
    }
    return LastTime;
  }
  
  public String getCurTime(Connection con)
  {
    String CurTime = null;
    try
    {
      Statement stmt = con.createStatement();
      String sql = "select to_char(sysdate,'" + this.oracleDateF2 + "') from dual\n";
      ResultSet rs = stmt.executeQuery(sql);
      if (rs.next()) {
        CurTime = rs.getString(1);
      }
      rs.close();
      stmt.close();
    }
    catch (Exception ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] getLastTime: SQLException: " + ex);
    }
    return CurTime;
  }
  
  public void updateLastTime(String CurTime, Connection con)
  {
    try
    {
      Statement stmt = con.createStatement();
      getClass();getClass();String sql = "insert into " + "it_promCRM_updCreateTime" + "\n" + "select MAX(id)+1,to_date('" + CurTime + "','" + this.oracleDateF2 + "') from " + "it_promCRM_updCreateTime" + "\n";
      stmt.executeUpdate(sql);
      stmt.close();
    }
    catch (Exception ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] getLastTime: SQLException: " + ex);
    }
  }
  
  public boolean isSupervisior(Connection con, String StaffID)
  {
    boolean isSupervisior = false;
    try
    {
      getClass();String sql = "select xf_privid from " + "xf_staff" + " where xf_staffcode=?\n";
      PreparedStatement pstmt = con.prepareStatement(sql);
      pstmt.setString(1, StaffID);
      ResultSet rs = pstmt.executeQuery();
      if ((rs.next()) && 
        (rs.getString("xf_privid").toUpperCase().equals("SU"))) {
        isSupervisior = true;
      }
      rs.close();
      pstmt.close();
    }
    catch (SQLException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] isSupervisior: SQLException: " + ex);
    }
    return isSupervisior;
  }
  
  public String getCustomerClass(String TxDate, String StoreCode, String DocNo, String Salesman, String IsItemRedeem, String IsPriceOverride, String VipGrade, String SKU, String PromID, Connection con)
  {
    String CustomerClass = null;
    if (isVIP7(con, TxDate, StoreCode, DocNo)) {
      CustomerClass = "VIP7";
    } else if (isVIP6Sku(con, SKU)) {
      CustomerClass = "VIP6";
    } else if ((VipGrade != null) && (!VipGrade.equals("")) && (!VipGrade.toUpperCase().equals("AP")) && (!VipGrade.toUpperCase().equals("Z1")) && (!VipGrade.toUpperCase().equals("ZP")) && (!VipGrade.toUpperCase().equals("ZS"))) {
      CustomerClass = getCustomerClassFromMapping(con, VipGrade);
    } else if (((PromID == null) || (PromID.equals(""))) && (IsItemRedeem.equals("0")) && (IsPriceOverride.equals("2")) && (!isSupervisior(con, Salesman))) {
      CustomerClass = "VIP3";
    } else if (((PromID == null) || (PromID.equals(""))) && (IsItemRedeem.equals("0")) && (IsPriceOverride.equals("2")) && (isSupervisior(con, Salesman))) {
      CustomerClass = "VIP5";
    } else {
      CustomerClass = "STD";
    }
    if ((CustomerClass == null) || (CustomerClass.equals(""))) {
      CustomerClass = "STD";
    }
    return CustomerClass;
  }
  
  public String getCustomerClassFromMapping(Connection con, String VipGrade)
  {
    String CustomerClass = null;
    try
    {
      getClass();String sql = "select customerclass from " + "it_vipgrade_map" + "\n" + "where vipgrade=?\n" + "and VIPGRADE not in ('AP','Z1','ZP')";
      PreparedStatement pstmt = con.prepareStatement(sql);
      pstmt.setString(1, VipGrade);
      ResultSet rs = pstmt.executeQuery();
      if (rs.next()) {
        CustomerClass = rs.getString("customerclass");
      }
      rs.close();
      pstmt.close();
    }
    catch (SQLException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] getCustomerClassFromMapping: SQLException: " + ex);
    }
    return CustomerClass;
  }
  
  public boolean isVIP6Sku(Connection con, String sku)
  {
    boolean isVIP6Sku = false;
    try
    {
      getClass();String sql = "select xf_plu from " + "xf_itemmas" + "\n" + " where (xf_group3 in ('2101','3100','3101','4100') or xf_group19='GC')\n" + " and xf_plu=?";
      PreparedStatement pstmt = con.prepareStatement(sql);
      pstmt.setString(1, sku);
      ResultSet rs = pstmt.executeQuery();
      if (rs.next()) {
        isVIP6Sku = true;
      }
      rs.close();
      pstmt.close();
    }
    catch (SQLException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] isVIP6Sku: SQLException: " + ex);
    }
    return isVIP6Sku;
  }
  
  public boolean isVIP7(Connection con, String TxDate, String StoreCode, String DocNo)
  {
    boolean isVIP7 = false;
    try
    {
      getClass();String sql = "select xf_txmsg from " + "xf_trans" + "\n" + " where xf_txdate=to_date(?,'" + this.oracleDateF + "') and xf_storecode=? and xf_docno=?\n" + " and xf_txtype=18";
      PreparedStatement pstmt = con.prepareStatement(sql);
      pstmt.setString(1, TxDate);
      pstmt.setString(2, StoreCode);
      pstmt.setString(3, DocNo);
      ResultSet rs = pstmt.executeQuery();
      while (rs.next())
      {
        String[] trans_msg = rs.getString("xf_txmsg").split(this.splitTxMsg);
        if (trans_msg.length <= 7)
        {
          if (trans_msg[1].toUpperCase().equals("C2")) {
            isVIP7 = true;
          }
        }
        else if ((trans_msg[1].toUpperCase().equals("C2")) || (trans_msg[7].toUpperCase().equals("C2"))) {
          isVIP7 = true;
        }
      }
      rs.close();
      pstmt.close();
    }
    catch (SQLException ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] isVIP7: SQLException: " + ex);
    }
    return isVIP7;
  }
  
  public void updateNegativeCoupon(Connection con)
  {
    Date dd = new Date();
    
    String sql = "DECLARE recCOUNT INT;\nbegin\nselect COUNT(*) into recCOUNT from it_vipitemdm_netnetsales i\ninner join \n(select distinct xf_txdate,xf_storecode,xf_docno,xf_saleslinenum from it_vipitemdm_netnetsales\nwhere xf_lastmodtime>=to_date(to_char(sysdate-1,'" + this.oracleDateF + "'),'" + this.oracleDateF + "')\n" + "and it_netnetcopy<0\n" + "and xf_qtysold>=0\n" + "and it_orig_netsales<>it_netnetcopy\n" + "order by xf_txdate,xf_storecode,xf_docno,xf_saleslinenum)\n" + "t on t.xf_txdate=i.xf_txdate and t.xf_storecode=i.xf_storecode and t.xf_docno=i.xf_docno and t.xf_saleslinenum=i.xf_saleslinenum\n" + ";\n" + "\n" + "IF recCOUNT>0\n" + "THEN\n" + "\tinsert into it_vipitemdm_negCpn_bk\n" + "\tselect i.* from it_vipitemdm_netnetsales i\n" + "\tinner join\n" + "\t(select distinct xf_txdate,xf_storecode,xf_docno,xf_saleslinenum from it_vipitemdm_netnetsales\n" + "\twhere xf_lastmodtime>=to_date(to_char(sysdate-1,'" + this.oracleDateF + "'),'" + this.oracleDateF + "')\n" + "\tand it_netnetcopy<0\n" + "\tand xf_qtysold>=0\n" + "\tand it_orig_netsales<>it_netnetcopy\n" + "\torder by xf_txdate,xf_storecode,xf_docno,xf_saleslinenum)\n" + "\tt on t.xf_txdate=i.xf_txdate and t.xf_storecode=i.xf_storecode and t.xf_docno=i.xf_docno and t.xf_saleslinenum=i.xf_saleslinenum\n" + "\torder by i.xf_txdate,i.xf_storecode,i.xf_docno,i.xf_saleslinenum\n" + "\t;\n" + "--------------------------------------------------------------------\n" + "\tMERGE into it_vipitemdm_netnetsales\n" + "\tusing\n" + "\t(select distinct xf_txdate,xf_storecode,xf_docno,xf_saleslinenum from it_vipitemdm_netnetsales\n" + "\twhere xf_lastmodtime>=to_date(to_char(sysdate-1,'" + this.oracleDateF + "'),'" + this.oracleDateF + "')\n" + "\tand it_netnetcopy<0\n" + "\tand xf_qtysold>=0\n" + "\tand it_orig_netsales<>it_netnetcopy\n" + "\torder by xf_txdate,xf_storecode,xf_docno,xf_saleslinenum)\n" + "\tt on (t.xf_txdate=it_vipitemdm_netnetsales.xf_txdate and t.xf_storecode=it_vipitemdm_netnetsales.xf_storecode and t.xf_docno=it_vipitemdm_netnetsales.xf_docno and t.xf_saleslinenum=it_vipitemdm_netnetsales.xf_saleslinenum)\n" + "\twhen matched then update\n" + "\tset it_vipitemdm_netnetsales.xf_amtsold=0, it_vipitemdm_netnetsales.it_netnetcopy=0\n" + "\t;\n" + "--------------------------------------------------------------------\n" + "\tdbms_output.put_line('['||to_char(sysdate,'" + this.oracleDateF2 + "')||']:\tUpdated '||to_char(recCOUNT)||' Records.');\n" + "ELSE\n" + "\tdbms_output.put_line('['||to_char(sysdate,'" + this.oracleDateF2 + "')||']:\tNo Records need to updated.');\n" + "END IF;\n" + "\n" + "end;\n";
    try
    {
      Statement stmt = con.createStatement();
      stmt.executeUpdate(sql);
      stmt.close();
      
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] updateNegativeCoupon: Update completed.");
    }
    catch (Exception ex)
    {
      getLogTime("\t[PromCrm_UpdateIsItemRedeem_Main] updateNegativeCoupon: SQLException: " + ex);
    }
  }
}
